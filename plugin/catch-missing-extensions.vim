" Catch opening of inexistent files ending in a dot. These are usually the
" result of the user hitting Enter too early during shell expansion when
" multiple files with the same prefix exist. In this case offer to open all
" files that match the pattern.
"
" Martin Rubli, 2010-07-27

augroup CatchMissingExtensions
	autocmd!
	" Note the 'nested' to make sure BufRead is executed again and syntax coloring is applied.
	" See: https://stackoverflow.com/questions/53797861
	autocmd BufNewFile *. nested :call HandleMissingExtensions()
augroup END

function! HandleMissingExtensions ()
	let new_file = expand("%:t")
	let pattern = new_file . "*"
	let pattern_files = glob(pattern, 1, 1)
	if len(pattern_files)
		if confirm("File '" . new_file . "' does not exist. Open the following files (" . pattern . ") instead?\n\n" . join(pattern_files, "\n") . "\n", "&Yes\n&No", 1, "Question") == 1
			:bd
			exec "args " . join(pattern_files, " ")
		endif
	endif
endfunction
